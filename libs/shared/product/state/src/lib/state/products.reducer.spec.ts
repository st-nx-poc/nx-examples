import { mockProducts } from '@nx-example/shared/product/data/testing';

import { initialState, productsReducer, ProductsState } from './products.reducer';

describe('Products Reducer', () => {
  let productsState: ProductsState;

  beforeEach(() => {
    productsState = {
      products: mockProducts
    };
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = productsReducer(productsState, action);

      expect(result).toBe(productsState);
      
      const defaultResult = productsReducer(undefined, {} as any);
      expect(defaultResult).toBe(initialState);
    });
  });
});
